$(window).on("load", function() {
    $(".loader .inner").fadeOut(700, function(){
        $(".loader").fadeOut(1000)
    });
})

var typed = new Typed(".typed",{
    strings: ["Fullstack Web Developer.", "Web Designer.", "Gamer."],
    typeSpeed: 60,
    loop: true,
    startDelay: 500,
    showCursor: true,
    cursorChar: '|',
    backDelay: 500,
    backSpeed: 60,
    smartBackspace: true
});

$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    // nav:true,
    dots:true,
    autoplay:true,
    autoplayTimeout:1000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:2
        },
        500:{
            items:3
        },
        800:{
            items:4
        },
        1000:{
            items:5
        }
    }
});

const nav = $("#navigation");
const navTop = nav.offset().top;
const navLink = $("#navigation li a");

$(window).on("scroll", stickyNavigation);
function stickyNavigation() {
    var body = $("body");

    if($(window).scrollTop() >= navTop) {
        body.css("padding-top", nav.outerHeight() + "px");
        body.addClass("fixedNav");
    } else {
        body.css("padding-top", 0);
        body.removeClass("fixedNav");
    }
}

$(navLink).click(function(e) {
    e.preventDefault();

    const targetElement = $(this).attr("href");
    const targetPosition = $(targetElement).offset().top;
    $("html, body").animate({ scrollTop: targetPosition - 50}, "slow")
})

$(".navbar-brand").click(function(e) {
    e.preventDefault();

    const targetElement = $(this).attr("href");
    const targetPosition = $(targetElement).offset().top;
    $("html, body").animate({ scrollTop: targetPosition - 100}, "slow")
})

$(".next-btn").click(function(e) {
    e.preventDefault();

    const targetElement = $(this).attr("href");
    const targetPosition = $(targetElement).offset().top;
    $("html, body").animate({ scrollTop: targetPosition - 50}, "slow")
})


//navbar
const about = $("#about-a")
const project = $("#project-a")
const skills = $("#skills-a")
const contact = $("#contact-a")
const navbarBrand = $(".navbar-brand")
const nextBtn = $(".next-btn")

$(about).click(function(e) {
    project.removeClass("active-nav")
    skills.removeClass("active-nav")
    contact.removeClass("active-nav")
    about.addClass("active-nav")
})

$(skills).click(function() {

    skills.addClass("active-nav")
    about.removeClass("active-nav")
    project.removeClass("active-nav")
    contact.removeClass("active-nav")
})

$(contact).click(function() {

    contact.addClass("active-nav")
    about.removeClass("active-nav")
    skills.removeClass("active-nav")
    project.removeClass("active-nav")
})

$(project).click(function() {

    project.addClass("active-nav")
    about.removeClass("active-nav")
    skills.removeClass("active-nav")
    contact.removeClass("active-nav")

})

$(nextBtn).click(function() {
    about.addClass("active-nav")
    skills.removeClass("active-nav")
    project.removeClass("active-nav")
    contact.removeClass("active-nav")
})
//reset active-nav
$(navbarBrand).click(function() {
    about.removeClass("active-nav")
    skills.removeClass("active-nav")
    project.removeClass("active-nav")
    contact.removeClass("active-nav")
})

$(window).scroll(function(){
    if ($(this).scrollTop() > 740) {
        about.addClass('active-nav')
        skills.removeClass("active-nav")
        project.removeClass("active-nav")
        contact.removeClass("active-nav")
    }
    if ($(this).scrollTop() > 1100 && $(this).scrollTop() <1800) {
        skills.addClass("active-nav")
        about.removeClass("active-nav")
        project.removeClass("active-nav")
        contact.removeClass("active-nav")
    }
    if ($(this).scrollTop() >= 1800) {
        project.addClass('active-nav')
        about.removeClass("active-nav")
        skills.removeClass("active-nav")
        contact.removeClass("active-nav")
    }

    if($(this).scrollTop() < 400) {
        about.removeClass("active-nav")
        skills.removeClass("active-nav")
        project.removeClass("active-nav")
        contact.removeClass("active-nav")
    }
});

// $(window).scroll(function() {
//     if($(window).scrollTop() + $(window).height() == $(document).height()) {
//         contact.addClass('active-nav')
//         about.removeClass("active-nav")
//         skills.removeClass("active-nav")
//         project.removeClass("active-nav")
//     }
//  });

